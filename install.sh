#!/usr/bin/env bash
echo "Updating our own directory"
git pull origin master

echo "Cloning/Updating assignment directory"
git clone https://git.uwaterloo.ca/krhancoc/cs350-assignments.git os161-container/assignments > /dev/null 2> /dev/null
cd os161-container/assignments > /dev/null
git pull origin master > /dev/null 2> /dev/null
cd - > /dev/null

echo "Building CS350 container"
docker build -t os161-runner ./os161-container

